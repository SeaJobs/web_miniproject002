//Boran Jork, [28 May 2021 at 23:54:38]:
import { Col, Container,Row,Form,Pagination} from 'react-bootstrap';
import MyNav from './components/MyNav';
import MyTable from './components/MyTable';
import { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';
import 'bootstrap/dist/css/bootstrap.min.css';
class App extends Component {
  constructor (props){
    super(props)
    this.state = {
      data : [
        {id : uuidv4().slice(0, 8),
        username : "", 
        Gender : "male",
        Email: "",
        Password : "",
        errorEmail:""
      }
      ],
    }

  }

  validateEmail=(e)=>{
    this.setState({
     [e.target.name]:e.target.value
    },()=>{
        let pattern = /^\S+@\S+\.[a-z]{3}$/g;
        let result = pattern.test(this.state.Email);
        if(result){
            this.setState({errorEmail:'',
            });
        }   
         else if(this.state.Email ===''){
             this.setState({errorEmail:"Email cannot be empty!"});
         }
      else{
             this.setState({  errorEmail:'Input invalid!'})     
         }

        let pattern2 = /^[a-zA-z]+$/;
            let result2 = pattern2.test(this.state.username)
            if(result2){
                this.setState({errorUsername:''})
            }
            else {
                this.setState({errorUsername:'input invalid!'})
            }
           

        }
    );        
 }

  render () {
    return (
      <div className="App">
         <MyNav/> 
          <Container>
            
             <Row>
                <Col lg = {8}>
                <br></br>
                  
                <h2>Personal Info</h2><br></br>
                
                      <Form>

                      <Form.Group className="mb-3" controlId="formGroupPassword">
                          <Form.Label name ="username" onChange = {this.validateEmail}>Username</Form.Label>
                          <Form.Control type="text" placeholder="username" />
                          <Form.Text className="text-muted" ><p style={{color:'red'}}> {this.state.errorUsername}</p></Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                        <Form.Label as="legend">Email address</Form.Label>
                        <Form.Control ref ="email" name="Email" onChange={this.validateEmail}
                        type="email" placeholder="Enter email" />
                        <Form.Text className="text-muted" ><p style={{color:'red'}}> {this.state.errorEmail}</p></Form.Text>
                        </Form.Group>
                      
                      </Form>
 
                </Col>
                <Col lg = {4}>
              
                <Form>
                    <div className="mb-3">
                      <br></br>
                      <br></br>
                      <br></br>
                      <br></br>
                      <h5>Gender</h5>
                      <Form.Check checked = "true" inline label="Male" name="group1" type="radio"/>
                      <Form.Check inline label="Female" name="group1" type="radio"/>
                    </div>
                    <div>
                    
                      <br></br>
                      <h5>Job</h5>
                      <Form.Check checked = "true" inline label="Student" name="group1" type="checkbox"/>
                      <Form.Check inline label="Teacher" name="group1" type="checkbox"/>
                      <Form.Check inline label="Developer" name="group1" type="checkbox"/>
                    </div>

                </Form>
                </Col> 
             </Row>

             <Row className="justify-content-end">
               <Col lg = {12} >
                        <button type="button" class="btn btn-success">Submit</button>
                        <button type="button"  class="btn btn-danger">Cancel</button>
               </Col>
                       
             </Row>

<Row>
             <Col lg = {12}>
                  <MyTable items = {this.state.data}/>
                </Col>
                
             </Row>
             <Row>
             <Col lg = {12}>
                <Pagination>
                    <Pagination.First />
                    <Pagination.Prev />
                    <Pagination.Item>{1}</Pagination.Item>
                    <Pagination.Ellipsis />

                    <Pagination.Item>{10}</Pagination.Item>
                    <Pagination.Item>{11}</Pagination.Item>
                    <Pagination.Item active>{12}</Pagination.Item>
                    <Pagination.Item>{13}</Pagination.Item>
                    <Pagination.Item disabled>{14}</Pagination.Item>
                    
                    <Pagination.Ellipsis />
                    <Pagination.Item>{20}</Pagination.Item>
                    <Pagination.Next />
                    <Pagination.Last />
                  </Pagination>
                </Col>
             </Row>
          </Container>
      </div>
    );
  }
}
export default App;