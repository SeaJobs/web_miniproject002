import moment from "moment";
import React from "react";
import localization from "moment/locale/km";
import { Table, Button } from "react-bootstrap";
import { v4 as uuidv4 } from "uuid";
moment.updateLocale("km", localization);
uuidv4(); // ⇨ '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed'

function MyTable(props) {
  return (
    <div className="my-5">
      <h3>Display Data As : </h3>
      <Button variant="primary" onClick={() => this.onAdd()}>
        Table
      </Button>
      <Button variant="success" onClick={() => this.onAdd()}>
        Card
      </Button>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>Gender</th>
            <th scope="col">Job</th>
            <th scope="col">Created</th>
            <th scope="col">Updated</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {props.items.map((item, idx) => (
            <tr key={idx}>
              <td>{item.id}</td>
              <td>{item.username}</td>
              <td>{item.Email}</td>
              <td>{item.Gender}</td>
              <td>{item.job}</td>
              <td>{moment(item.created_date).fromNow()}</td>
              <td>{moment(item.updated_date).fromNow()}</td>
              <td>
                <button type="button" class="btn btn-success">
                  View
                </button>
                <button type="button" class="btn btn-warning">
                  Update
                </button>
                <button type="button" class="btn btn-danger">
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}

export default MyTable;
