import React, { Component } from 'react'
import {Navbar} from 'react-bootstrap';

export default class MyNav extends Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark" ClassName = "me-auto">
            <Navbar.Brand href="#home">Personal Info react Bootstrap</Navbar.Brand>
            </Navbar>
        )
    }
}
